package com.devcamp.arrayflterinputrestapi.controller;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.arrayflterinputrestapi.service.ArrayIntService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ArrayIntController {
    @Autowired
    private ArrayIntService arrayIntService;

    @GetMapping("/array-int-request-query")
    public ArrayList<Integer> fillterQueryIntegers(@RequestParam(value = "query") int pos){
        return arrayIntService.filterQuery(pos);
    } 
    @GetMapping("array-int-param/{index}")
    public Integer filterIndexParam(@PathVariable(value = "index") int index){
        return arrayIntService.filterIndex(index);
    } 
}
