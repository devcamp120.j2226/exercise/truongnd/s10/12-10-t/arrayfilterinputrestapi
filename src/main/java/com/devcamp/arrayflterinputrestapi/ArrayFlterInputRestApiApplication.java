package com.devcamp.arrayflterinputrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArrayFlterInputRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArrayFlterInputRestApiApplication.class, args);
	}

}
