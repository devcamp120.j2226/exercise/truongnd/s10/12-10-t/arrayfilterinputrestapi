package com.devcamp.arrayflterinputrestapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class ArrayIntService {
    private int[] rainbows = {1, 23, 32, 43, 54, 65, 86, 10,15,16,18};

    public ArrayList<Integer> filterQuery(int pos){
        ArrayList<Integer> result = new ArrayList<>();

        for (Integer numbers : rainbows) {
            if(numbers > pos){
                result.add(numbers);
            }
        }
        return result;
    }
    public Integer filterIndex(int indexParam){
        int result = -1;
        if(indexParam >= 0 && indexParam <= 10){
            result = this.rainbows[indexParam];
        }
        return result;
    }
}
